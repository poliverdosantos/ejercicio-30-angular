import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boton',
  templateUrl: './boton.component.html',
  styleUrls: ['./boton.component.css']
})
export class BotonComponent implements OnInit {
  tamanio:number = 30;
  text:string = '';
  textError:string = '-Limite...!!!'

  constructor() { }

  ngOnInit(): void {
  }

  aumentar(){
    this.tamanio = this.tamanio + 5
    console.log(this.aumentar);
    
    if (this.tamanio === 500){
      this.text = this.textError
    }
  }

  reducir(){
    this.tamanio = this.tamanio -5
    if(this.tamanio <= 5  ){
      this.text = this.textError
    }else if(this.tamanio > 5){
      this.text
    }
  }

}
